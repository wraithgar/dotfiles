#
# Command specific completions for the nvm script
#

#nvm help                    Show this message
complete -c nvm -a help -d "Display help"
#nvm --version               Print out the latest released version of nvm
complete -c nvm -l version -d "Print out the latest released version of nvm"
#nvm install [-s] <version>  Download and install a <version>, [-s] from source. Uses .nvmrc if available
#nvm uninstall <version>     Uninstall a version
#nvm use <version>           Modify PATH to use <version>. Uses .nvmrc if available
#nvm run <version> [<args>]  Run <version> with <args> as arguments. Uses .nvmrc if available for <version>
#nvm current                 Display currently activated version
#nvm ls                      List installed versions
#nvm ls <version>            List versions matching a given description
#nvm ls-remote               List remote versions available for install
#nvm deactivate              Undo effects of NVM on current shell
#nvm alias [<pattern>]       Show all aliases beginning with <pattern>
#nvm alias <name> <version>  Set an alias named <name> pointing to <version>
#nvm unalias <name>          Deletes the alias named <name>
#nvm copy-packages <version> Install global NPM packages contained in <version> to current version
