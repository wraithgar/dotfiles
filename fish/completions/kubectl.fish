#
# Command specific completions for kubctl
#
set __k8s_timeout "--request-timeout=$FISH_KUBECTL_COMPLETION_TIMEOUT"
set -q FISH_KUBECTL_COMPLETION_TIMEOUT; or set FISH_KUBECTL_COMPLETION_TIMEOUT 5s

function __fish_kubectl -d "Call kubectl with a timeout"
  command kubectl $__k8s_timeout $argv
end

function __kubectl_get_context -d 'Gets the context for the current command'
  set -l cmd (commandline -opc)
  if [ (count $cmd) -eq 0 ]
    echo ""
    return 0
  else
    set -l foundContext 0
    for c in $cmd
      test $foundContext -eq 1
      and echo "$c"
      and return 0
      if contains -- $c "--context"
        set foundContext 1
      end
    end

    return 1
  end
end

function __kubectl_get_namespace -d "Get namespace for the current command"
  set -l cmd (commandline -opc)
  if [ (count $cmd) -eq 0 ]
    echo ""
    return 0
  else
    set -l foundContext 0
    for c in $cmd
      test $foundContext -eq 1
        and echo "$c"
        and return 0
      #starts with -n but is not -n in and of itself
      string match -qr -- '^-n' $c
        and not string match -q -- '^-n' $c
        and echo (string sub -s3 -- $c)
        and return 0
      if contains -- $c "--namespace" "-n"
        set foundContext 1
      end
    end

    return 1
  end
end

function __kubectl_get_resource -d "Get resource for the current command"
  set -l cmd (commandline -opc)
  if [ (count $cmd) -eq 0 ]
    echo ""
    return 0
  else
    for c in $cmd
      if contains -- $c (__kubectl_resource_kinds)
        echo "$c"
        return 0
      end
    end
    return 1
  end
end

set __kubectl_output_formats \
  json \
  yaml \
  wide \
  name \
  custom-columns= \
  custom-columns-file= \
  go-template= \
  go-template-file= \
  jsonpath= \
  jsonpath-file= \

set __kubectl_commands \
  create \
  expose \
  run \
  set \
  explain \
  get \
  edit \
  delete \
  rollout \
  scale \
  autoscale \
  certificate \
  cluster-info \
  top \
  cordon \
  uncordon \
  drain \
  taint \
  describe \
  logs \
  attach \
  exec \
  port-forward \
  proxy \
  cp \
  auth \
  apply \
  patch \
  replace \
  wait \
  convert \
  label \
  annotate \
  completion \
  alpha \
  api-resources \
  api-versions \
  config \
  plugin \
  version

function __kubectl_contexts -d "Get names of kubectl contexts"
  __fish_kubectl config get-contexts -oname
end

function __kubectl_namespaces -d "Get names of kubectl namespaces, optionally for context specified"
  set cmd (commandline -po)
  set -l context (__kubectl_get_context)
  if test -z "$context"
    set context "--context=$context"
  else
    __fish_kubectl get namespaces -oname $context|cut -d'/' -f2
  end
end

function __kubectl_resource_kinds -d "Get kinds of kubectl resources, optionally for context specified"
  set cmd (commandline -po)
  set context (__kubectl_get_context)
  if test -n "$context"
    set context "--context=$context"
  end
  __fish_kubectl api-resources -- "$context"|awk '{print $(NF)}'|sed 1d|string lower
end

function __kubectl_log_resource_kinds -d "Get kinds of kubectl resources, optionally for context specified, terminated by a / for use in log command"
  __kubectl_resource_kinds| string replace -r '$' '/'
end

function __kubectl_needs_command -d 'Test if a kubectl can be given next'
  set -l cmd (commandline -poc)
  for i in $cmd
    if contains -- $i $__kubectl_commands
      return 1
    end
  end
  #If we are in the middle of specifying a flag we don't want to show commands
  if string match -qr -- "^-" $cmd[-1]
    return 1
  end
  return 0
end

function __kubectl_no_resource -d 'Test if kubectl has yet to be given a resource'
  set cmd (commandline -po)
  for kind in (__kubectl_resource_kinds)
    if contains -- $kind $cmd
      return 1
    end
  end
  return 0
end

function __kubectl_using_option -d 'Test if option is currently the last item on the commandline'
  set cmd (commandline -poc)
  set query "("(string join -- "|" $argv)")"
  if test (count $cmd) -gt 1
    if string match -qr -- $query $cmd[-1]
      return 0
    end
  end
  return 1
end

#logs arguments require the slash, which is why this is a separate function than anything else that gets resources of a given type
#also default to pod type so we list pods in the initial completion
function __kubectl_get_log_resources -d "Get resources for given kind, optionally for context specified, output in the form of kind/name for use in log command"
  set cmd (commandline -po)
  set context (__kubectl_get_context)
  set namespace (__kubectl_get_namespace)
  set type (string match -r ".*\/" $cmd[3])
  if test -n $type
    set type (string replace "/" "" $type)
  end
  if test -z "$type"
    set type "pod"
  end
  if test -n "$context"
    set context "--context=$context"
  end
  if test -n "$namespace"
    set namespace "-n$namespace"
  end
  __fish_kubectl get $type "-ocustom-columns=KIND:kind,NAME:metadata.name" "--no-headers=true" $context $namespace|awk '{print tolower($1)"/"$2}'
end

function __kubectl_get_resources -d "Get resources for given kind, optionally for context and/or namespace specified"
  set cmd (commandline -po)
  set context (__kubectl_get_context)
  set namespace (__kubectl_get_namespace)
  set resource (__kubectl_get_resource)
  if test -z "$resource"
    return 1
  end
  if test -n "$context"
    set context "--context=$context"
  end
  if test -n "$namespace"
    set namespace "-n$namespace"
  end
  __fish_kubectl get $resource "-ocustom-columns=NAME:metadata.name" "--no-headers=true" $context $namespace
end

# The following options can be passed to any command:
complete -c kubectl -f -l "alsologtostderr" -r -a "false true" -d "log to standard error as well as files"
complete -c kubectl -f -l "as" -d "Username to impersonate for the operation"
complete -c kubectl -f -l "as-group" -d "Group to impersonate for the operation, this flag can be repeated to specify multiple groups."
complete -c kubectl -f -l "cache-dir" -d "Default HTTP cache directory"
complete -c kubectl -f -l "certificate-authority" -d "Path to a cert file for the certificate authority"
complete -c kubectl -f -l "client-certificate" -d "Path to a client certificate file for TLS"
complete -c kubectl -f -l "client-key" -d "Path to a client key file for TLS"
complete -c kubectl -f -l "cluster" -d "The name of the kubeconfig cluster to use"

#--context
complete -c kubectl -l "context" -d "The name of the kubeconfig context to use"
complete -c kubectl -f -n " __kubectl_using_option --context" -a "(__kubectl_contexts)"

complete -c kubectl -f -l "insecure-skip-tls-verify" -r -a "false true" -d "If true, the server's certificate will not be checked for validity. This will make your HTTPS connections insecure"
complete -c kubectl -l "kubeconfig" -d "Path to the kubeconfig file to use for CLI requests."
complete -c kubectl -f -l "log-backtrace-at" -d "when logging hits line file:N, emit a stack trace"
complete -c kubectl -l "log-dir" -d "If non-empty, write log files in this directory"
complete -c kubectl -f -l "log-flush-frequency" -d "Maximum number of seconds between log flushes"
complete -c kubectl -f -l "logtostderr" -d "log to standard error instead of files"
complete -c kubectl -f -l "match-server-version" -r -a "false true" -d "Require server version to match client version"

#-n --namespace
complete -c kubectl -s n -l "namespace" -d "If present, the namespace scope for this CLI request"
complete -c kubectl -f -n " __kubectl_using_option -n --namespace" -a "(__kubectl_namespaces)"

complete -c kubectl -f -l "request-timeout" -d "The length of time to wait before giving up on a single server request. Non-zero values should contain a corresponding time unit (e.g. 1s, 2m, 3h). A value of zero means don't timeout requests."
complete -c kubectl -f -s s -l "server" -d "The address and port of the Kubernetes API server"
complete -c kubectl -f -l "stderrthreshold" -d "logs at or above this threshold go to stderr"
complete -c kubectl -f -l "token" -d "Bearer token for authentication to the API server"
complete -c kubectl -f -l "user" -d "The name of the kubeconfig user to use"
complete -c kubectl -f -s v -l "v"  -d "log level for V logs"
complete -c kubectl -f -l "vmodule" -d "comma-separated list of pattern=N settings for file-filtered logging"

# Basic Commands (Beginner):
# create
complete -c kubectl -f -n '__kubectl_needs_command' -a 'create' -d 'Create a resource from a file or from stdin.'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'expose' -d 'Take a replication controller, service, deployment or pod and expose it as a new Kubernetes Service'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'run' -d 'Run a particular image on the cluster'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'set' -d 'Set specific features on objects'

# Basic Commands (Intermediate):
complete -c kubectl -f -n '__kubectl_needs_command' -a 'explain' -d 'Documentation of resources'
#get
complete -c kubectl -f -n '__kubectl_needs_command' -a 'get' -d 'Display one or many resources'
complete -c kubectl -n '__fish_seen_subcommand_from get' -s o -l 'output' -d 'Output format'
complete -c kubectl -f -n "__fish_seen_subcommand_from get; and __kubectl_using_option -o --output" -a "$__kubectl_output_formats"

complete -c kubectl -f -n '__kubectl_needs_command' -a 'edit' -d 'Edit a resource on the server'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'delete' -d 'Delete resources by filenames, stdin, resources and names, or by resources and label selector'

# Deploy Commands:
complete -c kubectl -f -n '__kubectl_needs_command' -a 'rollout' -d 'Manage the rollout of a resource'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'scale' -d 'Set a new size for a Deployment, ReplicaSet, Replication Controller, or Job'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'autoscale' -d 'Auto-scale a Deployment, ReplicaSet, or ReplicationController'

# Cluster Management Commands:
complete -c kubectl -f -n '__kubectl_needs_command' -a 'certificate' -d 'Modify certificate resources.'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'cluster-info' -d 'Display cluster info'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'top' -d 'Display Resource (CPU/Memory/Storage) usage.'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'cordon' -d 'Mark node as unschedulable'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'uncordon' -d 'Mark node as schedulable'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'drain' -d 'Drain node in preparation for maintenance'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'taint' -d 'Update the taints on one or more nodes'

# Troubleshooting and Debugging Commands:
complete -c kubectl -f -n '__kubectl_needs_command' -a 'describe' -d 'Show details of a specific resource or group of resources'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'logs' -d 'Print the logs for a container in a pod'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'attach' -d 'Attach to a running container'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'exec' -d 'Execute a command in a container'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'port-forward' -d 'Forward one or more local ports to a pod'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'proxy' -d 'Run a proxy to the Kubernetes API server'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'cp' -d 'Copy files and directories to and from containers.'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'auth' -d 'Inspect authorization'

# Advanced Commands:
complete -c kubectl -f -n '__kubectl_needs_command' -a 'apply' -d 'Apply a configuration to a resource by filename or stdin'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'patch' -d 'Update field(s) of a resource using strategic merge patch'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'replace' -d 'Replace a resource by filename or stdin'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'wait' -d 'Experimental: Wait for a specific condition on one or many resources.'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'convert' -d 'Convert config files between different API versions'

# Settings Commands:
complete -c kubectl -f -n '__kubectl_needs_command' -a 'label' -d 'Update the labels on a resource'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'annotate' -d 'Update the annotations on a resource'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'completion' -d 'Output shell completion code for the specified shell (bash or zsh)'

# Other Commands:
complete -c kubectl -f -n '__kubectl_needs_command' -a 'alpha' -d 'Commands for features in alpha'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'api-resources' -d 'Print the supported API resources on the server'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'api-versions' -d 'Print the supported API versions on the server, in the form of "group/version"'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'config' -d 'Modify kubeconfig files'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'plugin' -d 'Provides utilities for interacting with plugins.'
complete -c kubectl -f -n '__kubectl_needs_command' -a 'version' -d 'Print the client and server version information'

# [create]
complete -c kubectl -f -n '__fish_seen_subcommand_from create' -l 'allow-missing-template-keys' -r -a 'false true' -d 'If true, ignore any errors in templates when a field or map key is missing in the template. Only applies to golang and jsonpath output formats.'
complete -c kubectl -f -n '__fish_seen_subcommand_from create' -l 'dry-run' -r -a 'false true' -d 'If true, only print the object that would be sent, without sending it.'
complete -c kubectl -f -n '__fish_seen_subcommand_from create' -l 'edit' -r -a 'false true' -d 'Edit the API resource before creating'
complete -c kubectl -n '__fish_seen_subcommand_from create' -s f -l 'filename' -r -d 'Filename, directory, or URL to files to use to create the resource'
complete -c kubectl -n '__fish_seen_subcommand_from create' -s o -l 'output' -d 'Output format'
complete -c kubectl -f -n "__fish_seen_subcommand_from create; and __kubectl_using_option -o --output" -a "$__kubectl_output_formats"
complete -c kubectl -f -n '__fish_seen_subcommand_from create' -s R -l 'recursive' -r -a 'false true' -d 'Process the directory used in -f, --filename recursively. Useful when you want to manage related manifests organized within the same directory.'
complete -c kubectl -f -n '__fish_seen_subcommand_from create' -l 'save-config' -r -a 'false true' -d 'If true, the configuration of current object will be saved in its annotation. Otherwise, the annotation will be unchanged. This flag is useful when you want to perform kubectl apply on this object in the future.'
complete -c kubectl -f -n '__fish_seen_subcommand_from create' -s l -l 'selector' -r -d "Selector (label query) to filter on, supports '=', '==', and '!='.(e.g. -l key1=value1,key2=value2)"
#TODO limit to -o parameters stated here
complete -c kubectl -f -n '__fish_seen_subcommand_from create' -l 'template' -r -d "Template string or path to template file to use when -o=go-template, -o=go-template-file. The template format is golang templates [http://golang.org/pkg/text/template/#pkg-overview]"
#TODO limit to edit=true
complete -c kubectl -f -n '__fish_seen_subcommand_from create' -l 'windows-line-endings' -r -a 'false true' -d 'Only relevant if --edit=true. Defaults to the line ending native to your platform'

#Examples from cli help
complete -c kubectl -f -n '__fish_seen_subcommand_from create; and __kubectl_no_resource' -a 'clusterrole' -d 'Create a ClusterRole.'
complete -c kubectl -f -n '__fish_seen_subcommand_from create; and __kubectl_no_resource' -a 'clusterrolebinding' -d 'Create a ClusterRoleBinding for a particular ClusterRole'
complete -c kubectl -f -n '__fish_seen_subcommand_from create; and __kubectl_no_resource' -a 'configmap' -d 'Create a configmap from a local file, directory or literal value'
complete -c kubectl -f -n '__fish_seen_subcommand_from create; and __kubectl_no_resource' -a 'deployment' -d 'Create a deployment with the specified name.'
complete -c kubectl -f -n '__fish_seen_subcommand_from create; and __kubectl_no_resource' -a 'job' -d 'Create a job with the specified name.'
complete -c kubectl -f -n '__fish_seen_subcommand_from create; and __kubectl_no_resource' -a 'namespace' -d 'Create a namespace with the specified name'
complete -c kubectl -f -n '__fish_seen_subcommand_from create; and __kubectl_no_resource' -a 'poddisruptionbudget' -d 'Create a pod disruption budget with the specified name.'
complete -c kubectl -f -n '__fish_seen_subcommand_from create; and __kubectl_no_resource' -a 'priorityclass' -d 'Create a priorityclass with the specified name.'
complete -c kubectl -f -n '__fish_seen_subcommand_from create; and __kubectl_no_resource' -a 'quota' -d 'Create a quota with the specified name.'
complete -c kubectl -f -n '__fish_seen_subcommand_from create; and __kubectl_no_resource' -a 'role' -d 'Create a role with single rule.'
complete -c kubectl -f -n '__fish_seen_subcommand_from create; and __kubectl_no_resource' -a 'rolebinding' -d 'Create a RoleBinding for a particular Role or ClusterRole'
complete -c kubectl -f -n '__fish_seen_subcommand_from create; and __kubectl_no_resource' -a 'secret' -d 'Create a secret using specified subcommand'
complete -c kubectl -f -n '__fish_seen_subcommand_from create; and __kubectl_no_resource' -a 'service' -d 'Create a service using specified subcommand.'
complete -c kubectl -f -n '__fish_seen_subcommand_from create; and __kubectl_no_resource' -a 'serviceaccount' -d 'Create a service account with the specified name'

#All resource kinds
complete -c kubectl -f -n '__fish_seen_subcommand_from create; and __kubectl_no_resource' -a "(__kubectl_resource_kinds)"

# [describe]
complete -c kubectl -f -n '__fish_seen_subcommand_from describe' -l 'all-namespaces' -r -a 'false true' -d 'If present, list the requested object(s) across allnamespaces. Namespace in current context is ignored even if specified with --namespace.'
complete -c kubectl -f -n '__fish_seen_subcommand_from describe' -l 'include-uninitialized' -r -a 'false true' -d 'If true, the kubectl command applies to uninitialized objects. If explicitly set to false, this flag overrides other flags that make the kubectl commands apply to uninitialized objects, e.g., "--all".  Objects with empty metadata.initializers are regarded as initialized.'
complete -c kubectl -n '__fish_seen_subcommand_from describe' -s f -l 'filename' -r -d 'Filename, directory, or URL to files to use to create the resource'
complete -c kubectl -f -n '__fish_seen_subcommand_from describe' -s R -l 'recursive' -r -a 'false true' -d 'Process the directory used in -f, --filename recursively. Useful when you want to manage related manifests organized within the same directory.'
complete -c kubectl -f -n '__fish_seen_subcommand_from describe' -s l -l 'selector' -r -d "Selector (label query) to filter on, supports '=', '==', and '!='.(e.g. -l key1=value1,key2=value2)"
complete -c kubectl -f -n '__fish_seen_subcommand_from describe' -l 'show-events' -r -a 'false true' -d 'If true, display events related ot the described object.'

complete -c kubectl -f -n '__fish_seen_subcommand_from describe; and __kubectl_no_resource' -a "(__kubectl_resource_kinds)"
complete -c kubectl -f -n '__fish_seen_subcommand_from describe; and not __kubectl_no_resource' -a "(__kubectl_get_resources)"

# [explain]
complete -c kubectl -f -n '__fish_seen_subcommand_from explain; and __kubectl_no_resource' -a "(__kubectl_resource_kinds)"
complete -c kubectl -f -n '__fish_seen_subcommand_from explain; and not __kubectl_no_resource' -a "(__kubectl_get_resources)"

# [get]
complete -c kubectl -f -n '__fish_seen_subcommand_from get; and __kubectl_no_resource' -a "(__kubectl_resource_kinds)"
complete -c kubectl -f -n '__fish_seen_subcommand_from get; and not __kubectl_no_resource' -a "(__kubectl_get_resources)"

# [logs]
complete -c kubectl -f -n '__fish_seen_subcommand_from logs' -l 'all-containers' -r -a 'false true' -d "Get all containers's logs in the pod(s)."
complete -c kubectl -f -n '__fish_seen_subcommand_from logs' -s c -l 'container' -r -d 'Print the logs of this container'
complete -c kubectl -f -n '__fish_seen_subcommand_from logs' -s f -l 'follow' -r -a 'false true' -d 'Specify if the logs should be streamed.'
complete -c kubectl -f -n '__fish_seen_subcommand_from logs' -l 'limit-bytes' -r -d 'Maximum bytes of logs to return. Defaults to no limit.'
complete -c kubectl -f -n '__fish_seen_subcommand_from logs' -l 'pod-running-timeout' -r -d 'The length of time (like 5s, 2m, or 3h, higher than zero) to wait until at least one pod is running'
complete -c kubectl -f -n '__fish_seen_subcommand_from logs' -s p -l 'previous' -r -a 'false true' -d 'If ture, print the logs for the previous instance of the conainer in a pod if it exists.'
complete -c kubectl -f -n '__fish_seen_subcommand_from logs' -s l -l 'selector' -r -d "Selector (label query) to filter on, supports '=', '==', and '!='.(e.g. -l key1=value1,key2=value2)"
complete -c kubectl -f -n '__fish_seen_subcommand_from logs' -l 'since' -r -d 'Only return logs newer than a relative duration like 5s, 2m, or 3h. Defaults to all logs. Only one of since-time / since may be used.'
complete -c kubectl -f -n '__fish_seen_subcommand_from logs' -l 'since-time' -r -d 'Only return logs after a specific date (RFC3339). Defaults to all logs. Only one of since-time / since may be used.'
complete -c kubectl -f -n '__fish_seen_subcommand_from logs' -l 'tail' -r -d 'Lines of recent log file to display. Defaults to -1 with no selector, showing all log lines otherwise 10, if a selector is provided.'
complete -c kubectl -f -n '__fish_seen_subcommand_from logs' -l 'timestamps' -r -a 'false true'  -d 'Include timestamps on each line in the log output'

complete -c kubectl -f -n '__fish_seen_subcommand_from logs; and __kubectl_no_resource' -a "(__kubectl_log_resource_kinds)"
complete -c kubectl -f -n '__fish_seen_subcommand_from logs' -a "(__kubectl_get_log_resources)"
