function __fish_k8s_prompt
  if not command -sq kubectl
    return 1
  end

  set -l current_context (kubectl config current-context)
  set -l current_namespace (kubectl config view -o=jsonpath="{.contexts[?(@.name==\"$current_context\")].context.namespace}")

  if test -z $current_context
    return 1
  end


  echo -n '['
  set_color $__fish_k8s_prompt_color_context
  echo -n $current_context"|"
  if test -z $current_namespace
    echo -n 'default'
  else
    echo -n $current_namespace
  end
  set_color normal
  echo -n ']'
end
