function ssht --description "ssh to a server via tmux"
  ssh $argv -t "tmux a || tmux || /bin/bash"
end

