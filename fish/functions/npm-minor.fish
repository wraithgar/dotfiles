function npm-minor --description "publish a minor version to git and npm in the cwd"
  git checkout master;
  and git pull;
  and npm version minor;
  and git push;
  and git push --tags;
  and npm publish
end

