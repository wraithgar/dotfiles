function npm-major --description "publish a major version to git and npm in the cwd"
  git checkout master;
  and git pull;
  and npm version major;
  and git push;
  and git push --tags;
  and npm publish
end

