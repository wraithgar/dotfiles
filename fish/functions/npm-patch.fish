function npm-patch --description "publish a patch version to git and npm in the cwd"
  git checkout master;
  and git pull;
  and npm version patch;
  and git push;
  and git push --tags;
  and npm publish
end
