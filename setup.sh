#!/bin/sh

DOTFILES=`pwd`

#mkdir -p ~/.vim/bundle/
#mkdir -p ~/.config/fish
#mkdir ~/.vim/colors/

# ln -s $DOTFILES/bash_profile ~/.bash_profile
ln -s $DOTFILES/gitconfig ~/.gitconfig
# ln -s $DOTFILES/eslint.json ~/.eslint.json
ln -s $DOTFILES/fish/config.fish ~/.config/fish
ln -s $DOTFILES/fish/functions/* ~/.config/fish/functions
ln -s $DOTFILES/fish/completions/* ~/.config/fish/completions
#ln -s $DOTFILES/vimcolors/* ~/.vim/colors/

#https://github.com/wraithgar/janus
# mkdir ~/.janus
# pushd ~/.janus
# git clone git@github.com:mattn/emmet-vim.git
# git clone git@github.com:martingms/vipsql.git

#install https://github.com/ryanoasis/nerd-fonts

#ln -s $DOTFILES/TIS-100 ~/.local/share
popd
