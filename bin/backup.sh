#!/bin/sh
echo "Rsync tasks to sd"
rsync -qcrv ~/.task/ /media/gar/ext128/tasks/
echo "pmaster thumb to s3"
aws s3 cp /media/gar/RED\ 8GB/pmaster.kdbx  s3://garsthumb --acl private --sse --profile backups
echo "gnucash sd to s3"
aws s3 cp /media/gar/ext128/talents.gnucash  s3://garsthumb --acl private --sse --profile backups
echo "tasks sd to s3"
aws s3 sync /media/gar/ext128/tasks/ s3://garsthumb/tasks/ --acl private --profile backups
echo "cleanup gnucash logs"
rm -f /media/gar/ext128/talents.gnucash.*.log
