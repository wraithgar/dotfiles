#!/bin/bash
#gsettings set org.gnome.desktop.screensaver picture-uri file:///home/gar/Pictures/Vladstudio\ Wallpapers/`ls ~gar/Pictures/Vladstudio\ Wallpapers/|shuf|head -1`

PID=$(pgrep -n gnome-session)
export DBUS_SESSION_BUS_ADDRESS=$(grep -z DBUS_SESSION_BUS_ADDRESS /proc/$PID/environ|cut -d= -f2-)
gsettings set org.gnome.desktop.screensaver picture-uri "file://`find ~gar/Pictures/Vladstudio\ Wallpapers/ ~gar/Pictures/BOTW ~gar/Pictures/Doom -type f|shuf|head -1`"
